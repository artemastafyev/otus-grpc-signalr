﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Grpc.Extensions;
using Otus.Teaching.PromoCodeFactory.Grpc.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;
using CustomerEntity = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Customer;

namespace Otus.Teaching.PromoCodeFactory.Grpc
{
    /// <summary>
    /// Для тестирования можно использовать bloomrpc
    /// </summary>
    public class CustomerService : Customer.CustomerBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<CustomerEntity> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(
            ILogger<CustomerService> logger,
            IRepository<CustomerEntity> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task GetCustomers(Empty request, IServerStreamWriter<CustomerShortResponse> responseStream, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersResponse = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            foreach (var customer in customersResponse)
            {
                await responseStream.WriteAsync(customer);
            }
        }

        public override async Task<CustomerResponse> GetCustomer(UUID request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.Value.ToGuid());

            Validate(customer);

            var response = CustomerMapper.MapFromEntity(customer);

            return response;
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.ToGuidList());

            CustomerEntity customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return CustomerMapper.MapFromEntity(customer);
        }

        public override async Task<CustomerResponse> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.Id.ToGuid());

            Validate(customer);

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.ToGuidList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return CustomerMapper.MapFromEntity(customer);
        }

        public override async Task<Empty> DeleteCustomer(UUID request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.Value.ToGuid());

            Validate(customer);

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        private static void Validate(CustomerEntity customer)
        {
            if (customer == null)
                throw new InvalidOperationException();
        }
    }
}
