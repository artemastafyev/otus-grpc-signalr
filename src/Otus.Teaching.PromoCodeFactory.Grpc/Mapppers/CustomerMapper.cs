﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using CustomerEntity = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Customer;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Mappers
{
    public class CustomerMapper
    {
        public static CustomerEntity MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, CustomerEntity customer = null)
        {
            if (customer == null)
            {
                customer = new CustomerEntity();
                customer.Id = Guid.NewGuid();
            }

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static CustomerResponse MapFromEntity(CustomerEntity customer)
        {
            var customerResponse = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            customerResponse.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }));

            return customerResponse;
        }
    }
}
