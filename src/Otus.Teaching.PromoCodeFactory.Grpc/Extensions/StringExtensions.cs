﻿using Google.Protobuf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Extensions
{
    public static class StringExtensions
    {
        public static Guid ToGuid(this string guid) => Guid.Parse(guid);

        public static List<Guid> ToGuidList(this RepeatedField<string> guids) =>
            guids.Select(prefId => Guid.Parse(prefId)).ToList();
    }
}
