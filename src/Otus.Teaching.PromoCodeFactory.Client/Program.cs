﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var hubConnection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/customer")
                .Build();

            await hubConnection.StartAsync();

            var result = await hubConnection.InvokeAsync<List<CustomerShortResponse>>("GetCustomers");

            Console.WriteLine("Список клиентов:");
            foreach (var item in result)
            {
                Console.WriteLine($"{item.LastName} {item.FirstName}");
            }
            Console.WriteLine();

            var itemId = result.First().Id;
            var customer = await hubConnection.InvokeAsync<CustomerResponse>("GetCustomer", itemId);

            Console.WriteLine("Клиент 1:");
            Console.WriteLine($"{customer.LastName} {customer.FirstName}");
            Console.WriteLine($"{customer.Email} {customer.FirstName}");
            Console.WriteLine($"{customer.Preferences.First().Name}");

            await hubConnection.StopAsync();

            Console.ReadLine();
        }
    }

    public class CustomerShortResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }

    public class CustomerResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<PreferenceResponse> Preferences { get; set; }
    }

    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
